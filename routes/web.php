<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*las dos siguientes rutas se han creado para el servicio de envio de notificaciones por correo*/
Route::post('send', ['as' => 'send', 'uses' => 'HomeController@send'] );
Route::get('contact', ['as' => 'contact', 'uses' => 'HomeController@contact'] );

Route::get('/set_language/{lang}','Controller@setLanguage')->name('set_language');
Route::get('login/{driver}','Auth\LoginController@redirectToProvider')->name('social_auth');
Route::get('login/{driver}/callback','Auth\LoginController@handleProviderCallback');
Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
//creamos un grupo de rutas, el cual nos permite realizar un conjunto de acciones
Route::group(['prefix' => 'courses'], function(){
	Route::get('/{course}','CourseController@show')->name('courses.detail');
	//Route::get('/{courses}','CourseController@show')->name('courses.detail2');
});

Route::group(['prefix' => 'subscriptions' ],function(){
	Route::get('/plans','SubscriptionController@plans')
	->name('subscriptions.plans');
	Route::post('/process_subscription','subscriptionController@processSubscription')
	->name('subscriptions.process_subscription');
});
Route::get('/images/{path}/{attachment}', function($path, $attachment) {
	$file = sprintf('storage/%s/%s', $path, $attachment);
	if(File::exists($file)) {
		return Image::make($file)->response();
	}
});