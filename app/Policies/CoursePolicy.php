<?php

namespace App\Policies;

use App\Role;
use App\User;
use App\Course;
use Illuminate\Auth\Access\HandlesAuthorization;

class CoursePolicy
{
    use HandlesAuthorization;

    /*Creamos un metodo para los cursos*/

    public function opt_for_course(User $user, Course $course){
        //si el usuario no es profesor ó el usuario no es el que imparte el curso
        //puede suscribirse
        return ! $user->teacher || $user->teacher->id !== $course->teacher_id;
    }
//puede suscribir a la plataforma
    public function subscribe(User $user){//para usar el método subscribed, se debe 
        //declarar el use Billable en la clase User.php
        return $user->role_id !== Role::ADMIN && ! $user->subscribed('main');
    }

    //para saber si el usuario se puede inscribir al curso

    public function inscribe(User $user,Course $course){
        return ! $course->students->contains($user->student->id);
    }

    /**
     * Determine whether the user can view the course.
     *
     * @param  \App\User  $user
     * @param  \App\Course  $course
     * @return mixed
     */
    public function view(User $user, Course $course)
    {
        //
    }

    /**
     * Determine whether the user can create courses.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the course.
     *
     * @param  \App\User  $user
     * @param  \App\Course  $course
     * @return mixed
     */
    public function update(User $user, Course $course)
    {
        //
    }

    /**
     * Determine whether the user can delete the course.
     *
     * @param  \App\User  $user
     * @param  \App\Course  $course
     * @return mixed
     */
    public function delete(User $user, Course $course)
    {
        //
    }

    /**
     * Determine whether the user can restore the course.
     *
     * @param  \App\User  $user
     * @param  \App\Course  $course
     * @return mixed
     */
    public function restore(User $user, Course $course)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the course.
     *
     * @param  \App\User  $user
     * @param  \App\Course  $course
     * @return mixed
     */
    public function forceDelete(User $user, Course $course)
    {
        //
    }
}
