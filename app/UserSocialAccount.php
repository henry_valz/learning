<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSocialAccount extends Model
{
	//tenemos que decirle a nuestro modelo que datos pueden ser pemitidos para realizar una inserción
	protected $fillable = ['user_id','provider','provider_uid'];

	//para que no inserte el created_at cuando se accesa desde una red social
	public $timestamps = false;
    //definimos la relación
    public function user(){
    	return $this->belongsTo(User::class);
    }
}
