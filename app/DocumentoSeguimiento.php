<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocumentoSeguimiento extends Model
{
    //
    protected $table = 'documento_seguimientos';
    protected $primarykey = 'id';

    public function operadores(){
    	return $this->belongsToMany(Operador::class,'operador_docseguimiento','iddocseguimiento','idoperador');
    }
}
