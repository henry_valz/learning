<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Operador extends Model
{
    //
    protected $table = 'operadors';
    protected $primarykey = 'id';
    public function documentosDeSeguimiento(){
    	return $this->belongsToMany(DocumentoSeguimiento::class,'operador_docseguimiento','idoperador','iddocseguimiento');
    }
}
