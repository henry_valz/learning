<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    //
    protected $primaryKey = 'idarticulo';
    public function autors(){
    	return $this->belongsToMany(Autor::class,'article_autor','idarticulo','idautor');
    }
}
