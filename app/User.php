<?php

namespace App;
use Laravel\Cashier\Billable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * App\User
 *
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @mixin \Eloquent
 * @property int $id
 * @property int $role_id
 * @property string $name
 * @property string $email
 * @property string|null $email_verified_at
 * @property string $password
 * @property string $picture
 * @property string $stripe_id
 * @property string $card_brand
 * @property string $card_last_four
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCardBrand($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCardLastFour($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePicture($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRoleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereStripeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 */
class User extends Authenticatable
{
    use Notifiable, Billable;

    //se agrega el siguiente metodo para tener acceso al usuario que se está intentado crear
    protected static function boot(){
        parent::boot();
        static::creating(function (User $user){
            //Validamos que no se esté ejecutando desde un terminal
            if(! \App::runningInConsole()){
                $user->slug = str_slug($user->name . " " . $user->last_name,"-");
            }
        });
    }


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function navigation(){
        //esta autenticado? 
        return auth()->check() ? auth()->user()->role->name : 'guest';
    }

    //definimos las tres relaciones
    //para acceder a los roles
    public function role(){
        return $this->belongsTo(Role::class);
    }

    //para acceder a los estudiantes
    public function student(){
        return $this->hasOne(Student::class);
    }

    //para acceder a los teachers
    public function teacher(){
        return $this->hasOne(Teacher::class);
    }
    
    //para saber con que red social ingreso el usuasrio

    public function socialAccount(){
        return $this->hasOne(UserSocialAccount::class);
    }
}
