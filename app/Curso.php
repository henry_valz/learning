<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Curso extends Model
{
    //
    protected $table = 'curso';
    protected $primaryKey = 'idcurso';
    public $timestamps = false;

    public function alumnos(){
    	return $this->belongsToMany(Alumno::class,'alumno_curso','idcurso','idalumno')
    	->withPivot('nota');
    }
}
