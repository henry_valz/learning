<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Course
 *
 * @mixin \Eloquent
 * @property int $id
 * @property int $teacher_id
 * @property int $category_id
 * @property int $level_id
 * @property string $name
 * @property string $description
 * @property string $slug
 * @property string|null $picture
 * @property string $status
 * @property int $previous_approved
 * @property int $previous_rejected
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereLevelId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course wherePicture($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course wherePreviousApproved($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course wherePreviousRejected($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereTeacherId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereUpdatedAt($value)
 */
class Course extends Model
{
    //agregamos constantes para el estado
    const PUBLISHED		= 1;
    const PENDING 		= 2;
    const REJECTED 		= 3;

    //usaremos el conteo de review y students, definido para mostrarlo en la funcion show de la clase CourseController
    protected $withCount = ['reviews','students'];

    //método para adjuntar documentos
    public function pathAttachment(){
        return "../storage/app/public/courses/" . $this->picture;
    }

    //al crear este método, ya no responde a la consulta por url cuando se pone el ID, se 
    //tiene que poner el slug
    /*public function getRouteKeyName(){
        return 'slug';
    }
*/

    public function category(){
    	return $this->belongsTo(category::class)->select('id','name');
    }


    public function goals(){
    	return $this->hasMany(Goal::class)->select('id','course_id','goal');
    }
    //en el modelo de Level, se debe crear una funcion opositora hasOne, para
    //complementar esta relación level====
    public function level(){
    	return $this->belongsTo(Level::class)->select('id','name');
    }

    public function reviews(){
    	return $this->hasMany(Review::class)->select('id','user_id','course_id','rating','comment','created_at');
    }
    //tiene que tener definida su inversa en el modelo requirement
    public function requirements(){
    	return $this->hasMany(Requirement::class)->select('id','course_id','requirement');
    }
	//esta es una relacion para una table intermedia de muchos a muchos
    //se tiene que armas la relación entre ambos modelos por ejemplo Student y Course
    public function students(){
    	return $this->belongsToMany(Student::class);
    }

    public function teacher(){
    	return $this->belongsTo(Teacher::class);
    }
    //modificamo el atributo rating siempre deben empezar por get----y terminar con attribute, 
    //para el caso el modo de acceso seria a custom_rating
    public function getCustomRatingAttribute(){
        return $this->reviews->avg('rating');
    }

    public function relatedCourses(){
        return Course::with('reviews')->whereCategoryId($this->category->id)
            ->where('id','!=',$this->id)
            ->latest()
            ->limit(6)
            ->get();
    }

}
