<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;

class Language
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    //método que se ejecuta por defecto al llamarse a este middleware
    public function handle($request, Closure $next)
    {
        //return $next($request);
        if(session('applocale')){
            $configLanguage = config('languages')[session('applocale')];//extraemos del archivo de configuracion
            setlocale(LC_TIME, $configLanguage[1] . '.utf8');//establecemos
            //formatear fechas con el idioma seleccionado
            Carbon::setLocale(session('applocale'));
            App::setLocale(session('applocale'));
        } else {
            session()->put('applocale',config('app.fallback_locale'));
            setLocale(LC_TIME,'es_ES.utf8');
            Carbon::setLocale(session('applocale'));
            App::setLocale(config('app.fallback_locale'));//atributo seteado en config.app
        }
        return $next($request);
    }
}
