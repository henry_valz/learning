<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
//importar la siguiente linea, sino mostrará error el Socialite
use Laravel\Socialite\Facades\Socialite;
use App\User;
use App\UserSocialAccount;
use App\Student;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    //sobreescribimos el método logout de la clase AuthenticatesUsers

    public function logout(Request $request){
        //cerramos la session del usuario
        auth()->logout();
        //borramos la session del usuario
        session()->flush();
        //hacemos un redirect a el login
        return redirect('/login');
    }

    public function redirectToProvider(string $driver){
        //redirect al usuario para pedir permisos de la aplicacion
        return Socialite::driver($driver)->redirect();
        //return $S;
    }

    public function handleProviderCallback(string $driver){
        //si el usuario preciona cancelar antes de autorizar el acceso
        //en el formulario de la red social
        if(! request()->has('code') || request()->has('denied')){
            session()->flash('message',['danger',__("Inicio de sessión cancelado")]);
            return redirect('login');
        }


        $socialUser = Socialite::driver($driver)->user();
        $user = null;
        $success = true;
        $email = $socialUser->email;
        $check = User::whereEmail($email)->first();
        //comprobamos si el usuario esta registrado
        if($check){
            $user = $check;
        } else {//si el usuario no está registrado, lo registramos
            \DB::beginTransaction();
            try {
                $user = User::create([
                    "name" => $socialUser->name,
                    "email" => $email]);

                //para realizar esta inserción en el modelo UserSocialAccount, debe setearse la propiedad filleable y especificar los campos a insertarse que serían los tres datos que se quieren insertar
                UserSocialAccount::create([
                    "user_id" => $user->id,
                    "provider" => $driver,
                    "provider_uid" => $socialUser->id]);
                Student::create([
                    "user_id" => $user->id]);
            } catch (Exception $e) {//si se produce algún error al momento de intentar registrar el usuario,
                //hacemos un rollback para cancelar los registrado
                $success = $e->getMessage();
                \DB::rollBack();
            }
        }
            if($success === true){//si el registro se realizó con éxito, hacemos commit e iniciamos session del nuevo usuario
                \DB::commit();
                //iniciar session en laravel
                auth()->loginUsingId($user->id);
                return redirect(route('home'));
            }
            //si en caso se produjera error
            session()->flash('message',['danger',$success]);
            return redirect('login');
        
    }
}
