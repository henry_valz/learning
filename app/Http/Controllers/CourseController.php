<?php

namespace App\Http\Controllers;
use App\Course;


class CourseController extends Controller
{
    //definimos el metodo para coger el detalle del curso
    //atrapa la misma variable que mandas
    public function show(Course $course){
    	//dd($la_variable_que_desees);
    	//si queremos cargar la información al objeto que ya tenemos debemos usar la funcion load y no with
    	//$model = $coure->with([
    	$course->load([
    		'category' => function($q){
    			$q->select('id','name');
    		},
    		'goals' => function($q){
    			$q->select('id','course_id','goal');
    		},
    		'level' => function($q){
    			$q->select('id','name');
    		},
    		'requirements' => function($q){
    			$q->select('id','course_id','requirement');
    		},
    		'reviews.user',
    		'teacher'
    		//esta linea no funciona si usamos el load, para ello debemos quitar esta linea y definir en la
    		//clase modelo la propiedad withCount
    	//])->withCount(['students','reviews'])->get();
    	])->get();

    	$related = $course->relatedCourses();
    	return view('courses.detail',compact('course','related'));
    }

}
