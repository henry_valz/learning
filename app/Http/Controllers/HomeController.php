<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //se ejecuta antes que cualquier otro proceso
        //y protege nuestra apliación a través de autenticación
        //podemos agregar una excepción
        //$this->middleware('auth')->except('index');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //acedemos a todos los cursos paginaodos y en estado pubicado
        $courses = Course::withCount(['students'])
            ->with('category','teacher','reviews')
            ->where('status',Course::PUBLISHED)
            ->latest()
            ->paginate(10);
            //dd($courses);
        return view('home',compact('courses'));
    }

    public function send(Request $request)
   {
       //guarda el valor de los campos enviados desde el form en un array
       $data = $request->all();

       //se envia el array y la vista lo recibe en llaves individuales {{ $email }} , {{ $subject }}...
       \Mail::send('mails.enviar_notificaciones', $data, function($message) use ($request)
       {
           //remitente
           $message->from($request->email, $request->name);

           //asunto
           $message->subject($request->subject);

           //receptor
           $message->to('valz.henk@gmail.com', 'nombre de usuario');

           $message->cc('henry_valz@hotmail.com','mensaje en copia');
           $message->bcc('kvalero@cenepred.gob.pe','mensaje en copia oculta');

       });
       return \View::make('success');
   }

   public function contact(){
    return \View::make('contact');
   }
}
