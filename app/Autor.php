<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Autor extends Model
{
    //
    protected $primaryKey = 'idautor';

    public function articles(){
    	return $this->belongsToMany(Article::class,'article_autor','idarticulo','idautor');
    }
}
