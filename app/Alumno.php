<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alumno extends Model
{
    //
    protected $table = 'alumno';
    protected $primaryKey = 'idAlumno';
    public $timestamps = false;

    public function cursos(){
    	return $this->belongsToMany(Curso::class,'alumno_curso','idalumno','idcurso')
    	->withPivot('nota')->as('cursos_alumno');
    }
}
