<?php

use Faker\Generator as Faker;

$factory->define(App\Category::class, function (Faker $faker) {
    return [
        //con estas lineas cargamos datos de prueba a la base de datos, son de manera temporal
        'name' => $faker->randomElement(['PHP','JAVASCRIPT','DISEÑO WEB','SERVIDORES','MYSQL']),
        'description' => $faker->sentence
    ];
});
