<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOperadorDocseguimiento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('operador_docseguimiento', function(Blueprint $table){
            $table->unsignedInteger('idoperador');
            $table->foreign('idoperador')->references('id')->on('operadors');
            $table->unsignedInteger('iddocseguimiento');
            $table->foreign('iddocseguimiento')->references('id')->on('documento_seguimientos');
            $table->string('tipodoc');
            $table->string('nombrefile');
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
