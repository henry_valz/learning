<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticleAutor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('article_autor',function(Blueprint $table){
            $table->unsignedInteger('idarticulo');
            $table->foreign('idarticulo')->references('idarticulo')->on('articles');
            $table->unsignedInteger('idautor');
            $table->foreign('idautor')->references('idautor')->on('autors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
