<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    //metodo para crear los objetos
    public function up()
    {
        Schema::create('roles',function(Blueprint $table){
            $table->increments('id');
            $table->string('name')->comment('Nombre del rol del usuario');
            $table->text('description');
            $table->timestamps();
        });
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');

            //agregramos el valor por default
            $table->unsignedInteger('role_id')->default(\App\Role::STUDENT);
            //agregamos una foranea campo-----------------id refer---tabla
            $table->foreign('role_id')->references('id')->on('roles');

            $table->string('name');
            $table->string('last_name')->nullable();
            $table->string('slug');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();//tiene que permitir nullos, ya que cuando accesan por redes sociales, no se almacena la contraseña
            //agregamos las columnas para cashier
            $table->string('picture')->nullable();
            $table->string('stripe_id')->nullable();
            $table->string('card_brand')->nullable();
            $table->string('card_last_four')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
        Schema::create('subscriptions',function(Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('name');
            $table->string('stripe_id');
            $table->string('stripe_plan');
            $table->integer('quantity');
            $table->timestamp('trial_ends_at')->nullable();
            $table->timestamp('ends_at')->nullable();
            $table->timestamps();
        });

//para los accesos con redes sociales
        Schema::create('user_social_accounts',function(Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('provider');//dato de la red social con la que se trabaje
            $table->string('provider_uid');//ide del usuario en la red social seleccionado
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    //metodo para destruir los objetos
    public function down()
    {
        Schema::dropIfExists('users');
        //agregamos aqui la tabla que quisieramos eliminar en el rollback
        Schema::dropIfExists('roles');
        Schema::dropIfExists('subscriptions');
        Schema::dropIfExists('user_social_accounts');
    }
}
