<header>
	<nav class="navbar navbar-expand-md navbar-light-light navbar-laravel">
		<div class="container">
			<a class="navbar-brand" href="{{ url('/') }}">
				{{ config('app.name') }}
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarContent">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarContent">
				<ul class="navbar-nav mr-auto">
					
				</ul>
				<!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                	<!-- para eso se valido en una sola linea para concatenar el blade con el navigation del usuario, aqui llamamos al resultado de la valiadacion de la autentiacion y mostrar el que corresponde -->
                @include('partials.navigations.' . \App\User::navigation())                    

                    <li class="nav-item dropdown">
                        <a
                            class="nav-link dropdown-toggle"
                            href="#"
                            id="navbarDropdownMenuLink"
                            data-toggle="dropdown"
                            aria-haspopup="true"
                            aria-expanded="false"
                        >
                            {{ __("Selecciona un idioma") }}
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a
                                class="dropdown-item"
                               href="{{ route('set_language', ['es']) }}"
                            >
                               {{ __("Español") }}
                            </a>
                            <a
                                    class="dropdown-item"
                                    href="{{ route('set_language', ['en']) }}"
                            >
                                {{ __("Inglés") }}
                            </a>
                        </div>
                    </li>
                </ul>
			</div>
		</div>		
	</nav>
</header>