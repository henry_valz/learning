<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>Llamado de emergencia</title>
</head>
<body>
<p>Estimados Señores </p>

<p>Gobierno Regional AREQUIPA</p>

<p>Gobiernos Locales de la Región AREQUIPA </p>

<p>Tenemos el agrado de dirigirnos a usted, para hacer de su conocimiento de las funciones otorgadas por la Ley N° 29664 y su reglamento, el CENEPRED ha elaborado el “ ESCENARIO DE RIESGOS ANTE LA TEMPORADA DE  LLUVIAS PARA LA SIERRA DEL 21 AL 24 DE OCTUBRE DE 2018”, con la colaboración de instituciones científicas y técnicas como: SENAMHI, INGEMMET e INEI.</p>

<p>El citado documento, es una aproximación del riesgo existente ante la probabilidad de ocurrencia de lluvias y/o Bajas Temperaturas, agradeceremos disponer la difusión del referido Escenario de Riego entre los Gobiernos Locales de su jurisdicción, y en especial comunicar a aquellos que se encuentren en Nivel de Riesgo “Muy Alto” y “Alto”, con la finalidad de analizar el contenido del mismo y determinar las acciones correspondientes a la gestión prospectiva, correctiva y reactiva que permitan proteger a la población expuesta. Adicionalmente, indicarles que esta información también se encuentra disponible en el Sistema de Información para la Gestión de Riesgos de Desastres - SIGRID.</p>

<p>DESCARGUE INFORMACIÓN DETALLADA DE SU REGIÓN ↓</p>

<p>La presente dirección electrónica es usada solo para fines de difusión informativa, cualquier consulta técnica que se requiera agradeceré comunicarla a los correos: soporte-sigrid@cenepred.gob.pe  ó  azambrano@cenepred.gob.pe ; o al teléfono 201-3550 anexo 127.</p>

</body>
</html>